<?php

namespace App\Classes;

/**
 * Class ParseHtml
 * @package App\Classes
 */
class ParseHtml
{
    public static $domain;

    protected static $url;
    protected static $html;

    /**
     * ParseHtml constructor.
     * @param string $link
     */
    public function __construct(string $link)
    {
        try
        {
            // Проверяем, пустая ли ссылка или нет
            if(!empty($link)){
                // Записываем переданную ссылку
                $this::$url = $link;
            }else{
                throw new \Exception(__METHOD__." | Url is empty!");
            }
        } catch (\Exception $exception)
        {
            echo $exception->getMessage();
            exit();
        }

        if(isset($this::$url)){
            $this->prepareHtml();
        }
    }

    /**
     * @return bool|string
     */
    private function prepareHtml()
    {
        try
        {
            // Проверяем длину ссылку
            if (strlen($this::$url) >= 12) {
                try
                {
                    // Проверяем, передали нам полную ссылку или нет
                    $fullUrl = preg_match("#http#", $this::$url);
                    if($fullUrl){

                        $domain = explode("/", $this::$url);

                        $this::$domain = $domain[2];

                        $ch = curl_init($this::$url);
                        // Сохраняем ответ в переменную
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        // Следуем за редиректами
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                        // Отключаем проверки, для загрузки страниц по https
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        $this::$html = curl_exec($ch);

                        curl_close($ch);

                        return $this::$html;
                    }else{
                        throw new \Exception(__METHOD__." | You have to put full link with \'http\' or \'https\' in the beginning!");
                    }
                } catch (\Exception $exception)
                {
                    echo $exception->getMessage();
                    exit();
                }
            }else{
                throw new \Exception(__METHOD__." | Minimum url length: 12 symbols!");
            }
        } catch (\Exception $exception)
        {
            echo $exception->getMessage();
            exit();
        }
    }
}